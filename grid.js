var Configuration = {
	selector: "",
	source: [],
	filteredData: [],
	paginatedData: [],
	gridData: [],
	sortableColumns: [],
	filterableColumns: [],
	isHeaderFixed: true,
	pagination: true,
	pageSize: 5,
	currentPage: 1,
	totalRecords: 0
};
var sort_by;

const Table = function (){
	var grid = document.createElement("table");
	
	//Header Fixed
	if(Configuration.isHeaderFixed)
		grid.className = "fixed-header";

	function Header(columns){
		
		function FilterPanel (){
			var tr = document.createElement("tr");
			tr.className = "filter-row";

			function addFilter(column){
				var filterCell = document.createElement("td");
				var filter = document.createElement("input");
				filter.type = "text";
				filter.placeholder = "All"
				filter.value = Configuration.filterableColumns.filter(_column => _column.name === column)[0].phrase
				filter.addEventListener("input", e => {DataTable.filterGrid(e, column)});
				filterCell.appendChild(filter)
				return filterCell;
			}

			columns.forEach(column => tr.appendChild(Configuration.filterableColumns.filter(_column => _column.name === column).length > 0 ? document.createElement("td").appendChild(addFilter(column)) : document.createElement("td")));
			return tr;
		}
		
		var headerRow = document.createElement('tr');
		columns.forEach(column => {
			let th = document.createElement("th")
			let span = document.createElement("span")
			span.innerHTML = column || '';
			th.appendChild(span);
			th.title = column || '';
			
			let cols = Configuration.sortableColumns.filter(_column => _column.name === column);
			if (cols[0]){
				let img = document.createElement("img");
				img.setAttribute("src", "images/unsort.png");

				let orderI = document.createElement("i");
					orderI.style.display = cols[0].order === 0 ? 'none' : 'block';

				th.addEventListener("click", e => {
					let order = Math.max.apply(Math, Configuration.sortableColumns.map(col => { return col.order; }));
					if(cols[0].isSorted)
						if (cols[0].asc){
							cols[0].asc = false;
							img.setAttribute("src", "images/desc.png");
						}
						else{
							cols[0].isSorted = false;
							cols[0].asc = true;
							cols[0].order = 0;
							img.setAttribute("src", "images/unsort.png");
						}
					else{
						cols[0].isSorted = true;
						cols[0].order = order + 1;
						img.setAttribute("src", "images/asc.png");
					}

					Configuration.sortableColumns = Configuration.sortableColumns.sort((a,b) => {return a.order - b.order});
					Configuration.sortableColumns.filter(col => col.isSorted).forEach((col, i) => {
						col.order = i+1;
					});

					DataTable.sort();
					
					orderI.innerText = cols[0].order === 0 ? '' : cols[0].order;
					orderI.style.display = cols[0].order === 0 ? 'none' : 'block';

					console.log(cols[0])
				}, true);
				th.appendChild(img);
				th.appendChild(orderI);
			}

			headerRow.appendChild(th);
		});

		var header = document.createElement("thead");
		header.appendChild(headerRow);
		header.appendChild(FilterPanel());
		return header;		
	}

	function Body(data){
		function Row(row){
			var tr = document.createElement("tr");
			row.forEach(item => {
				let td = document.createElement("td");
				td.innerText = item || '';
				td.title = item || '';
				tr.appendChild(td);
			});
			return tr;
		}

		var gridBody = document.createElement("tbody");
		data.forEach(row => {
			gridBody.appendChild(Row(Object.values(row)));
		});
		return gridBody;
	}

	grid.appendChild(Header(Object.keys(Configuration.source[0])));
	grid.appendChild(Body(Configuration.gridData));
	return grid;
}

var Pagination = function() {
	var paginationContainer = document.createElement("div");
	paginationContainer.className = "pagination-container";
	paginationContainer.id = "pagination-container";

	// Pagination Status
	var paginationStatus = document.createElement("p");
	paginationStatus.innerText = "Showing " + (((Configuration.currentPage - 1) * Configuration.pageSize) + 1) + " to " + ((Configuration.currentPage * Configuration.pageSize > Configuration.totalRecords ? Configuration.totalRecords : Configuration.currentPage * Configuration.pageSize)) + " of " + Configuration.totalRecords + " entries";
	paginationContainer.appendChild(paginationStatus);

	var paginationControl = document.createElement("ul");
	paginationControl.className = "pagination-control";

	function Previous(){
		var previous = document.createElement("li");
		previous.innerText = "Previous";
		previous.addEventListener("click", e => {
			if(Configuration.currentPage > 1)
				Configuration.currentPage--;
		});
		return previous;
	}

	function Next(){
		var next = document.createElement("li");
		next.innerText = "Next";
		next.addEventListener("click", e => {
			if(Configuration.currentPage < Math.ceil(Configuration.totalRecords/Configuration.pageSize))
				Configuration.currentPage++;
		});
		return next;
	}

	var Pages = function(){
		var totalPages = Math.ceil(Configuration.totalRecords/Configuration.pageSize);

		paginationControl.appendChild(Previous());

		for (let i = 0,p = Configuration.currentPage; i < 5;) {
			(function(){
				if(p <= totalPages){
					var pi = p;
					let page = document.createElement("li");
					page.innerText = p;
					if(Configuration.currentPage == pi){
						page.className = "active";
					}
					page.addEventListener("click",e => {console.log(pi);Configuration.currentPage = pi});
					paginationControl.appendChild(page);					
				}
			}());
			i++;
			p++;
		}
	
		paginationControl.appendChild(Next());
	}();
	
	paginationControl.addEventListener("click", DataTable.paginate);

	paginationContainer.appendChild(paginationControl);
	return paginationContainer;
}


var DataTable = function(){
	return {
		init: function(configuration) {
			Configuration.selector = configuration.selector || document.body;
			Configuration.source = configuration.source || [];
			Configuration.filteredData = configuration.source || [];
			Configuration.gridData = configuration.source || [];
			Configuration.totalRecords = configuration.source.length || 0;
			Configuration.isHeaderFixed = configuration.isHeaderFixed && true;
			Configuration.pagination = configuration.pagination && true;
			Configuration.pageSize = configuration.pageSize || 5;
			Configuration.currentPage = configuration.currentPage || 1;

			//init filterable columns
			configuration.filterableColumns = configuration.filterableColumns || ["name", "alpha3Code", "subregion", "area", "flag"];
			configuration.filterableColumns.forEach(column => {
				let _column = {
					name: column,
					phrase: ""
				};
				Configuration.filterableColumns.push(_column);
			});

			//init sortable columns
			configuration.sortableColumns = configuration.sortableColumns || ["name", "alpha3Code", "subregion", "area", "flag"];
			configuration.sortableColumns.forEach(column => {
				let _column = {
					name: column,
					isSorted: false,
					asc: true,
					order: 0
				};
				if(typeof Configuration.source[0][column] === "string")
					_column.type = "string"
				if(typeof Configuration.source[0][_column.name] === "number")
					_column.type = "number"
				Configuration.sortableColumns.push(_column);
			});

			document.getElementById(Configuration.selector).appendChild(Table());

			if(Configuration.pagination){
				document.getElementById(Configuration.selector).appendChild(Pagination());
				this.paginate();
			}
		},
		refresh: function() {
			var grid = document.getElementById(Configuration.selector);
			var table = Table();
			var node = table.getElementsByTagName("tbody")[0];
			grid.childNodes[0].replaceChild(node, grid.getElementsByTagName("tbody")[0]);

			if (Configuration.pagination) {
				grid.replaceChild(Pagination(), grid.getElementsByClassName("pagination-container")[0]);
			}
		},
		filterGrid: function(e, column) {
			Configuration.currentPage = 1;
			Configuration.filterableColumns.filter(_column => _column.name === column)[0].phrase = e.target.value;
			Configuration.filteredData = Configuration.source;
			Configuration.filterableColumns.forEach(_column => {
				if(typeof Configuration.source[0][_column.name] === "string")
					Configuration.filteredData = Configuration.filteredData.filter(row => row[_column.name].includes(_column.phrase))
				if(typeof Configuration.source[0][_column.name] === "number" && _column.phrase.length > 0)
					Configuration.filteredData = Configuration.filteredData.filter(row => row[_column.name] == _column.phrase)
			})
			this.sort();
		},
		sort: function() {
			var sortedFields = [];
			Configuration.currentPage = 1;
			Configuration.sortableColumns.filter(_col => _col.isSorted ).forEach(_col => {
				let obj = {};
				obj.name = _col.name;
				obj.primer = _col.type === "string" ? (v) => {return v.toUpperCase()} : parseInt;
				obj.reverse = !_col.asc;
				sortedFields.push(obj);
			});
			Configuration.filteredData.sort(sort_by(sortedFields));

			if(Configuration.pagination)
				this.paginate();
			else
				Configuration.gridData = Configuration.filteredData;
		},
		paginate: function() {
			Configuration.totalRecords = Configuration.filteredData.length;
			Configuration.paginatedData = [];
			var start = (Configuration.currentPage - 1) * Configuration.pageSize + 1;
			var end = Configuration.currentPage * Configuration.pageSize;
			if (end > Configuration.totalRecords) {
				end = Configuration.totalRecords;
			}
			for(let i = start; i <= end; i++){
				Configuration.paginatedData.push(Configuration.filteredData[i-1]);
			}
			Configuration.gridData = Configuration.paginatedData;
			DataTable.refresh();
		}
	}
}();



//Helpers
// Sorting Algorithm
// Algorithm adapted from Stackoverflow stackoverflow.com/questions/6913512/how-to-sort-an-array-of-objects-by-multiple-fields
(function() {
    // utility functions
    var default_cmp = function(a, b) {
            if (a == b) return 0;
            return a < b ? -1 : 1;
        },
        getCmpFunc = function(primer, reverse) {
            var dfc = default_cmp, // closer in scope
                cmp = default_cmp;
            if (primer) {
                cmp = function(a, b) {
                    return dfc(primer(a), primer(b));
                };
            }
            if (reverse) {
                return function(a, b) {
                    return -1 * cmp(a, b);
                };
            }
            return cmp;
        };

    // actual implementation
    sort_by = function(sortedFields) {
        var fields = [],
            n_fields = sortedFields.length,
            field, name, reverse, cmp;

        // preprocess sorting options
        for (var i = 0; i < n_fields; i++) {
            field = sortedFields[i];
            if (typeof field === 'string') {
                name = field;
                cmp = default_cmp;
            }
            else {
                name = field.name;
                cmp = getCmpFunc(field.primer, field.reverse);
            }
            fields.push({
                name: name,
                cmp: cmp
            });
        }

        // final comparison function
        return function(A, B) {
            var a, b, name, result;
            for (var i = 0; i < n_fields; i++) {
                result = 0;
                field = fields[i];
                name = field.name;

                result = field.cmp(A[name], B[name]);
                if (result !== 0) break;
            }
            return result;
        }
    }
}());